resource "aws_iam_role" "ec2-admin-instance-role" {
    name                = "ec2-admin-instance-role"
    path                = "/"
    assume_role_policy  = "${data.aws_iam_policy_document.ec2-instance-policy.json}"
}

data "aws_iam_policy_document" "ec2-instance-policy" {
    statement {
        actions = ["sts:AssumeRole"]

        principals {
            type        = "Service"
            identifiers = ["ec2.amazonaws.com"]
        }
    }
}

resource "aws_iam_role_policy_attachment" "ecs-instance-role-attachment" {
    role       = "${aws_iam_role.ec2-admin-instance-role.name}"
    policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_instance_profile" "ec2-admin-instance-profile" {
    name = "ecs-instance-profile"
    path = "/"
    role = "${aws_iam_role.ec2-admin-instance-role.name}"
}